import React from 'react';
import './styles/header.css'

class Header extends React.Component {
    render() {
        return (
            <div className='header navbar shadow'>
                <span className='header-title'>
                    <i className="fas fa-icons me-1"/>
                    {this.props.title ? this.props.title : 'Chat'}</span>
                <span className='header-users-count'>
                    {this.props.usersCount} participants
                    <i className="fas fa-users ms-1"/>
                </span>
                <span className='header-messages-count'>
                    {this.props.messagesCount} messages
                    <i className="fas fa-comments ms-1"/>
                </span>
                <span className='header-last-message-date'>
                    <i className="fas fa-history me-1"/>
                    last message at {this.props.lastMessageDate}
                </span>
            </div>
        );
    }
}

export default Header;