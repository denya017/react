import React from 'react'
import './styles/message.css'

class OwnMessage extends React.Component {

    deleteMessage = (event) => {
        this.props.deleteFunction(this.props.id);
        event.preventDefault();
    }

    render() {
        return (
            <div className='own-message d-flex '>
                <div>
                    <div className='message-user-name'>{this.props.user}</div>
                    <div className='message-text'>{this.props.text}</div>
                    <span className='message-time'>
                        {new Date(this.props.createdAt).toTimeString().substr(0, 5)}
                    </span>
                    <button className='message-edit'>
                        <i className="fas fa-edit"/>
                    </button>
                    <button className='message-delete' onClick={this.deleteMessage}>
                        <i className="fas fa-trash"/>
                    </button>
                </div>
            </div>
        );
    }
}

export default OwnMessage;