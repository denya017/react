import React from 'react';
import './styles/message.css'

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isLike: false}
        this.likeMessage = this.likeMessage.bind(this);
    }

    likeMessage() {
        let status = this.state.isLike;
        this.setState({isLike: !status});
    }

    render() {
        let iClass;
        if(this.state.isLike) {
            iClass = 'fas fa-heart';
        }
        else iClass = 'far fa-heart';
        return (
            <div className='message d-flex'>
                <img src={this.props.avatar} alt="" className='message-user-avatar'/>
                <div>
                    <div className='message-user-name'>{this.props.user}</div>
                    <div className='message-text'>{this.props.text}</div>
                    <span className='message-time'>
                        {new Date(this.props.createdAt).toTimeString().substr(0,5)}
                    </span>
                    <button className='message-like' onClick={this.likeMessage}>
                        <i className={iClass}/>
                    </button>
                </div>
            </div>
        );
    }
}

export default Message;