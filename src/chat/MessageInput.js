import React from 'react'
import './styles/common.css'

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {textMessage: ''}
        this.sendMessage = this.sendMessage.bind(this);
    }
    onChange(e) {
        const value = e.target.value;
        this.setState({textMessage: value});
    }
    sendMessage = (event) => {
        if (this.state.textMessage !== '') {
            const dateSending = new Date();
            this.props.parentCallback({
                id: dateSending.getTime(),
                userId: "1",
                avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
                user: "Me",
                text: this.state.textMessage,
                createdAt: dateSending.toISOString(),
                editedAt: ""
            }, 'add');
            this.setState({textMessage: ''});
        }
        event.preventDefault();
    }

    render() {
        return (
            <div className='message-input d-flex'>
                <textarea id="input" className="form-control" rows="3" value={this.state.textMessage} onChange={(e) => this.onChange(e)}/>
                <div className='d-flex align-items-center'>
                    <button type="submit" className="btn btn-primary d-flex flex-nowrap align-items-center ms-4" onClick={this.sendMessage}>
                        <i className="fas fa-paper-plane me-2"/>
                        Send
                    </button>
                </div>
            </div>
        );
    }
}

export default MessageInput;