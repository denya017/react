import React from 'react';
import Header from "./Header";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import Preloader from "./Preloader";

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {messages: {}, isFetching: true, error: null};
    }

    componentDidMount() {
        fetch(this.props.url)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                this.setState({messages: data, isFetching: false})
            })
            .catch(e => {
                console.log(e);
                this.setState({
                    isFetching: false, error: {
                        status: e.status,
                        type: e.type
                    }
                });
            });
    }

    countAmount(messages, key) {
        let buf = [];
        let counter = 0;
        for (let message of messages) {
            if (!buf.includes(message[key])) {
                counter++;
                buf.push(message[key]);
            }
        }
        return counter;
    }

    maxDate(messages) {
        let dates = [];
        for (let message of messages) {
            dates.push(new Date(message['createdAt']));
        }
        const maxDate = new Date(Math.max.apply(null, dates));
        let result = '';
        if (maxDate.toDateString() !== new Date().toDateString()) {
            let day = maxDate.getDate().toString(),
                month = (maxDate.getMonth() + 1).toString();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            result += day + '.' + month;
            if (maxDate.getFullYear() !== new Date().getFullYear()) {
                result += '.' + maxDate.getFullYear();
            }
            result += ' ';
        }
        result += maxDate.getHours() + ':' + maxDate.getMinutes();
        return result;
    }

    handleCallback = (childData, action) =>{
        let msg = this.state.messages;
        if (action === 'add') {
            msg.push(childData);
        }
        if (action === 'delete') {
            for (let message of msg) {
                if (message.id === childData) {
                    msg.splice(msg.indexOf(message),1);
                    break;
                }
            }
        }
        this.setState({messages: msg})
    }

    render() {
        if (this.state.isFetching) return(
            <Preloader/>
        );

        if (this.state.error) return <div>{`Error: ${this.state.error.status}, type: ${this.state.error.type}`}</div>;

        return (
            <div className='chat'>
                <Header
                    usersCount={this.countAmount(this.state.messages, 'userId')}
                    messagesCount={this.state.messages.length}
                    lastMessageDate={this.maxDate(this.state.messages)}
                />
                <MessageList
                    messages={this.state.messages}
                    parentCallback={this.handleCallback}
                />
                <MessageInput
                    parentCallback = {this.handleCallback}
                />
            </div>
        );
    }
}

export default Chat;